package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;

public class ImageSerializerBase64Impl implements ImageSerializer {
    @Override
    public String serialize(File image) throws IOException {
        byte[] fileContent = Files.readAllBytes(image.toPath());
        return Base64.getEncoder().encodeToString(fileContent);
    }

    @Override
    public byte[] deserialize(String encodedImage) {
        return Base64.getDecoder().decode(encodedImage);
    }
}
